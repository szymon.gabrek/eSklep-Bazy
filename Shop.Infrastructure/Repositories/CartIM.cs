﻿using Shop.Domain.Model.Cart;
using Shop.Domain.Model.Cart.Repositories;
using Shop.GenericComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Infrastructure
{
    public class CartIM : ICartRepository
    {
        private List<Cart> carts = new List<Cart>();

        public CartIM()
        {
            carts = new List<Cart> {
                new Cart { Id = Guid.NewGuid() ,TotalCost = 150.55m },
                new Cart { Id = Guid.NewGuid() ,TotalCost = 99.99m },
            };
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Cart Find(int id)
        {
            throw new NotImplementedException();
        }

        public List<Cart> FindAll()
        {
            return carts;
        }

        public void Insert(Cart cart)
        {
            carts.Add(cart);
        }
    
    
    }
}
