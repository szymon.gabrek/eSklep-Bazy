﻿using Shop.Domain.Model.Customer;
using Shop.Domain.Model.Customer.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Infrastructure
{
    public class CustomerIM : ICustomerRepository
    {
        private List<Customer> customers = new List<Customer>();

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Customer Find(int id)
        {
            throw new NotImplementedException();
        }

        public List<Customer> FindAll()
        {
            return customers;
        }

        public void Insert(Customer customer)
        {
            customers.Add(customer);
        }
    }
}
