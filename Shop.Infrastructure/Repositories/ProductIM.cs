﻿using Shop.Domain.Model.Product;
using Shop.Domain.Model.Product.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Infrastructure.Repositories
{
    public class ProductIM : IProductRepository
    {
        private List<Product> products = new List<Product>();

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Product Find(int id)
        {
            throw new NotImplementedException();
        }

        public List<Product> FindAll()
        {
            return products;
        }

        public void Insert(Product product)
        {
            products.Add(product);
        }
    }
}
