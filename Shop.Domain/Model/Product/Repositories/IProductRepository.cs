﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.GenericComponent;

namespace Shop.Domain.Model.Product.Repositories
{
    public interface IProductRepository : IGenericComponent<Product>
    {
    }
}
