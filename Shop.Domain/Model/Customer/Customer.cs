﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Domain.Model.Customer
{
    public class Customer
    {
        public virtual int Id { get;  set; }
        public virtual string FirstName { get;  set; }
        public virtual string LastName { get;  set; }
        public virtual string Email { get;  set; }
        public virtual string Password { get;  set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Country { get;  set; }

        public virtual void ChangeEmail(string email)
        {
            if (this.Email != email)
            {
                this.Email = email;
            }
        }

        public static Customer Create(int id, string firstname, string lastname, string email, string country)
        {
            if (string.IsNullOrEmpty(firstname))
                throw new ArgumentNullException("firstname");

            if (string.IsNullOrEmpty(lastname))
                throw new ArgumentNullException("lastname");

            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException("email");

            if (country == null)
                throw new ArgumentNullException("country");

            Customer customer = new Customer()
            {
                Id = id,
                FirstName = firstname,
                LastName = lastname,
                Email = email,
                Country = country
            };

            return customer;
        }

    }
}
