﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.GenericComponent;

namespace Shop.Domain.Model.Customer.Repositories
{
    public interface ICustomerRepository : IGenericComponent<Customer>
    {
    }
}
