﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.Product.Repositories;
using Shop.Domain.Model.Customer;
using Shop.Domain.Model.Customer.Repositories;


namespace Shop.Domain.Model.Cart
{
    enum Status { in_progress, ordered, delivered}

    public class Cart
    {
        public Guid Id { get; set; }
        public int CustomerId { get; set; }

        public decimal TotalCost;

        Status Status;

        public IList<Product.Product> cartProducts { get; set; }

        public static Cart Create(Customer.Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            Cart cart = new Cart();
            cart.Id = Guid.NewGuid();
            cart.CustomerId = customer.Id;
            cart.Status = Status.in_progress;

          
            return cart;
        }

        public virtual void Add(Product.Product cartProduct)
        {
            if (cartProduct == null)
                throw new ArgumentNullException();


            this.cartProducts.Add(cartProduct);
        }

        public virtual bool Remove(Product.Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

           
            return this.cartProducts.Remove(product);
            
        }

        public virtual void Clear()
        {
            this.cartProducts.Clear();
        }

        

    }
}
