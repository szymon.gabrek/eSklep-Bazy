﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.GenericComponent;

namespace Shop.Domain.Model.Cart.Repositories
{
    public interface ICartRepository : IGenericComponent<Cart>
    {
        
    }
}
