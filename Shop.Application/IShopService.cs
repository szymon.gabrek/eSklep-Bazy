﻿using Shop.Domain.Model.Product;
using Shop.Domain.Model.Cart;
using Shop.Domain.Model.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Application
{
    public interface IShopService
    {
        IList<Product> GetProducts();
        void CreateNewProduct(Product product);

        IList<Cart> GetCarts();
        void CreateNewCart(Cart cart);

        IList<Customer> GetCustomers();
        void CreateNewCustomer(Customer customer);
    }
}
