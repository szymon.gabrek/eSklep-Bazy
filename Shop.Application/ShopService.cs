﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Infrastructure;
using Shop.Domain.Model.Cart.Repositories;
using Shop.Domain.Model.Cart;
using Shop.Domain.Model.Customer;
using Shop.Domain.Model.Customer.Repositories;
using Shop.Domain.Model.Product;
using Shop.Domain.Model.Product.Repositories;


namespace Shop.Application
{
    public class ShopService : IShopService
    {
        private ICartRepository cartRepository;
        private IProductRepository productRepository;
        private ICustomerRepository customerRepository;

        public ShopService() {
            cartRepository = new CartIM();
            productRepository = new ProductIM();
        }


        public void CreateNewCart(Cart cart)
        {
        
        }

        public void CreateNewCustomer(Customer customer)
        {
            throw new NotImplementedException();
        }

        public void CreateNewProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public IList<Cart> GetCarts()
        {
            throw new NotImplementedException();
        }

        public IList<Customer> GetCustomers()
        {
            throw new NotImplementedException();
        }

        public IList<Product> GetProducts()
        {
            throw new NotImplementedException();
        }
    }
}
