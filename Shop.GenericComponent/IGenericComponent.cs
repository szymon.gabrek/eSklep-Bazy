﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.GenericComponent
{
    public interface IGenericComponent<T>
    {
         void Insert(T genericComponent);

         void Delete(int id);

         T Find(int id);

         List<T> FindAll();
    }
}
