﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.Domain.Model.Cart.Repositories;
using Moq;

namespace Shop.Application.UnitTests
{
    [TestClass]
    public class CartServiceTests
    {
        [TestMethod]
        public void CheckFindMethodCalled()
        {
            // Arrange
            Mock<IPostRepository> repositoryMock = new Mock<IPostRepository>();
            IBlogService bs = new BlogService(repositoryMock.Object);

            // Act
            bs.GetAllPosts();

            // Assert
            repositoryMock.Verify(k => k.FindAll(), Times.Once());
        }

    }
}
